# TkMyPastebin
# Copyright (C) 2018 -- 2020 Petr Vojkovský <pevo@protonmail.com>

lappend auto_path [file join "."]
package require Tk
package require ::mypastebin::config
package require ::mypastebin::fraCollect
package require ::mypastebin::fraTagManager

proc main {argc argv} {
	if {![file exists [dict get $::mypastebin::config::dConf DBF]]} {
		tk_messageBox -type ok -icon error -title "Info"\
			-message "DB not found."
		exit
	};# fi
	initGui
}

proc initGui {} {
	wm protocol . WM_DELETE_WINDOW {tkmypastebinFileExit}
	wm title . "TkMyPastebin"

	menu .menu
	. config -menu .menu
	foreach m {File Help} {
		set $m [menu .menu.[string tolower $m] -tearoff 0]
		.menu add cascade -label $m -underline 0 -menu [set $m]
	};# hcaerof
	$File add command -underline 1 -label "Exit" -command tkmypastebinFileExit 
	$Help add command -label About -command tkmypastebinHelpAbout

	pack [ttk::notebook .nb] -fill both -expand 1
	.nb add [::mypastebin::fraCollect::gui] -text "Pastes"
	.nb add [::mypastebin::fraTagManager::gui] -text "Tags"
}

proc tkmypastebinHelpAbout {} {
	if {[info exists ::VERSION]} {
		set verze $::VERSION
	} else {
		if {[catch {open "../VERSION" r} chFin]} {
			tk_messageBox -type ok \
				-message "Nepodařilo se otevřít soubor ${fname}.\n$chFin" \
				-icon warning -title "Upozornění"
		} else {
			set verze [read $chFin]
			close $chFin
		}; # fi-esle {[catch {open "../VERSION" r} chFin]}
	}; # fi-esle {[info exists ::VERSION]}
	tk_messageBox -type ok -icon info -title "About" \
		-message "TkMyPastebin\n\nverion $verze
			\nby Petr Vojkovský
			\nTkMyPastebin is open source\nand freely distributable."
}

proc tkmypastebinFileExit {} {
	exit
}

# Execute the main procedure.
main $argc $argv
