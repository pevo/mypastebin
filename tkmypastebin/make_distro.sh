#!/bin/sh
# Create only one file for distribution/release.
version=`cat VERSION`
fout="tkmypastebin-$version.tcl"
echo "# TkMypastebin" > $fout
echo "# Copyright (C) 2018 -- 2020 Petr Vojkovský <pevo@protonmail.com>" >> $fout
echo "set ::VERSION $version" >> $fout
cat src/config.tcl >> $fout
cat src/paste.tcl >> $fout
cat src/tag.tcl >> $fout
cat src/fraTagToPaste.tcl >> $fout
cat src/fraPaste.tcl >> $fout
cat src/fraCollect.tcl >> $fout
cat src/fraTag.tcl >> $fout
cat src/fraTagManager.tcl >> $fout
cat src/tkmypastebin.tcl >> $fout

